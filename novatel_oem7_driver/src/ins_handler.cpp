////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2020 NovAtel Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

#include <novatel_oem7_driver/oem7_message_handler_if.hpp>

#include <driver_parameter.hpp>



#include <novatel_oem7_driver/oem7_ros_messages.hpp>
#include <novatel_oem7_driver/oem7_messages.h>
#include <novatel_oem7_driver/oem7_imu.hpp>

#if __has_include("tf2_geometry_msgs/tf2_geometry_msgs.hpp")
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#else
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#endif
#include "sensor_msgs/msg/imu.hpp"

#include "novatel_oem7_msgs/msg/corrimu.hpp"
#include "novatel_oem7_msgs/msg/imuratecorrimu.hpp"
#include "novatel_oem7_msgs/msg/insstdev.hpp"
#include "novatel_oem7_msgs/msg/insconfig.hpp"
#include "novatel_oem7_msgs/msg/inspva.hpp"
#include "novatel_oem7_msgs/msg/inspvax.hpp"

#include <oem7_ros_publisher.hpp>
#include <driver_parameter.hpp>

#include <math.h>
#include <map>


namespace novatel_oem7_driver
{
  /***
   * Converts degrees to Radians
   *
   * @return radians
   */
  inline double degreesToRadians(double degrees)
  {
    return degrees * M_PIf64 / 180.0;
  }

  const double DATA_NOT_AVAILABLE = -1.0; ///< Used to initialized unpopulated fields.

  class INSHandler: public Oem7MessageHandlerIf
  {
    rclcpp::Node* node_;

    std::unique_ptr<Oem7RosPublisher<sensor_msgs::msg::Imu>>                   imu_pub_;
    std::unique_ptr<Oem7RosPublisher<sensor_msgs::msg::Imu>>                   raw_imu_pub_;
    std::unique_ptr<Oem7RosPublisher<novatel_oem7_msgs::msg::CORRIMU>>         corrimu_pub_;
    std::unique_ptr<Oem7RosPublisher<novatel_oem7_msgs::msg::INSSTDEV>>        insstdev_pub_;
    std::unique_ptr<Oem7RosPublisher<novatel_oem7_msgs::msg::INSPVAX>>         inspvax_pub_;
    std::unique_ptr<Oem7RosPublisher<novatel_oem7_msgs::msg::INSCONFIG>>       insconfig_pub_;


    std::shared_ptr<novatel_oem7_msgs::msg::INSPVA>   inspva_;
    std::shared_ptr<novatel_oem7_msgs::msg::CORRIMU>  corrimu_;
    std::shared_ptr<novatel_oem7_msgs::msg::INSSTDEV> insstdev_;

    oem7_imu_rate_t imu_rate_;                ///< IMU output rate
    double imu_raw_gyro_scale_factor_;        ///< IMU-specific raw gyroscope scaling
    double imu_raw_accel_scale_factor_;       ///< IMU-specific raw acceleration scaling.
    std::vector<std::vector<double>> static_meas_{};
    double bias_raw_imu_rot_[3] = {0.0, 0.0, 0.0};
    double bias_raw_imu_acc_[3] = {0.0, 0.0, 0.0};
    bool init_raw_calibration_lin_;
    bool init_raw_calibration_ang_;

    std::string frame_id_;

    typedef std::map<std::string, std::string> imu_config_map_t;
    imu_config_map_t imu_config_map;

    std::unique_ptr<DriverParameter<std::string>> imu_rate_p_;
    std::unique_ptr<DriverParameter<std::string>> imu_desc_p_;

    oem7_imu_rate_t getImuRate(oem7_imu_type_t imu_type)
    {
      static DriverParameter<int> rate_p("supported_imus." + std::to_string(imu_type) + ".rate", 0, *node_);
      return rate_p.value();
    }

    void getImuDescription(oem7_imu_type_t imu_type, std::string& desc)
    {
      static DriverParameter<std::string> desc_p("supported_imus." + std::to_string(imu_type) + ".name", "", *node_);
      desc = desc_p.value();
    }

    void doInitRawCalibration(const RAWIMUSXMem& raw)
    {
      // intercept the first N messages for calibration.
      // This formulation allows co-calibration across multiple units running this same piece of driver and the same N,
      // TODO: although the actual implementation is not there yet.
      const unsigned int calib_len = 200;
      // single-unit calibration. The unit should be stationary and upright during this period.
      if (static_meas_.size() < calib_len){
        static_meas_.push_back(std::vector<double>{
          static_cast<double>(raw.x_acc),
          static_cast<double>(raw.y_acc),
          static_cast<double>(raw.z_acc),
          static_cast<double>(raw.x_gyro),
          static_cast<double>(raw.y_gyro),
          static_cast<double>(raw.z_gyro),
        });
      } else {
        std::vector<double> avg = std::reduce(static_meas_.begin(), static_meas_.end(),
            std::vector<double>{0.,0.,0.,0.,0.,0.},
            // define the vector add function
            [](std::vector<double> &x, std::vector<double> &y){
              std::vector<double> result;
              result.reserve(x.size());
              std::transform(x.begin(), x.end(), y.begin(), 
                   std::back_inserter(result), std::plus<double>());
              return result;
            }
        );
        std::for_each(avg.begin(), avg.end(), [](double &v){ v /= static_cast<double>(calib_len); });
        // TODO: maybe we can also calculate variance?
        if (init_raw_calibration_lin_){
          bias_raw_imu_acc_[0] = avg[0];
          bias_raw_imu_acc_[1] = avg[1];
          bias_raw_imu_acc_[2] = avg[2] - ONE_G / imu_raw_accel_scale_factor_;
          init_raw_calibration_lin_ = false;
        }
        if (init_raw_calibration_ang_){
          bias_raw_imu_rot_[0] = avg[3];
          bias_raw_imu_rot_[1] = avg[4];
          bias_raw_imu_rot_[2] = avg[5];
          init_raw_calibration_ang_ = false;
        }
        static_meas_.clear(); // frees up memory.
      }
    }


    void processInsConfigMsg(Oem7RawMessageIf::ConstPtr msg)
    {
      std::shared_ptr<novatel_oem7_msgs::msg::INSCONFIG> insconfig;
      MakeROSMessage(msg, insconfig);
      insconfig_pub_->publish(insconfig);

      const oem7_imu_type_t imu_type = static_cast<oem7_imu_type_t>(insconfig->imu_type);

      std::string imu_desc;
      getImuDescription(imu_type, imu_desc);
      
      if(imu_rate_ == 0) // No override; this is normal.
      {
        imu_rate_ = getImuRate(imu_type);
      }

      if(imu_rate_ == 0) // No rate configured at all.
      {
        RCLCPP_ERROR_STREAM(node_->get_logger(),
                    "IMU type = '" << imu_type  << "': IMU rate unavailable. IMU output disabled.");
        return;
      }

      // imu_raw_accel_scale_factor_ = getImuLinearScale(imu_type);
      // imu_raw_gyro_scale_factor_ = getImuAngularScale(imu_type);

      if(imu_raw_gyro_scale_factor_  == 0.0 ||
         imu_raw_accel_scale_factor_ == 0.0) // No override, this is normal.
      {
        if(!getImuRawScaleFactors(
            imu_type,
            imu_raw_gyro_scale_factor_,
            imu_raw_accel_scale_factor_))
        {
          RCLCPP_ERROR_STREAM(node_->get_logger(), 
            "IMU type= '" << insconfig->imu_type << "'; Scale factors unavilable. Raw IMU output disabled");
          return;
        }
      }

      RCLCPP_INFO_STREAM(node_->get_logger(),
                         "IMU: "          << imu_type  << " '"  << imu_desc << "' "
                      << "rate= "         << imu_rate_                      << "' "
                      << "gyro scale= "   << imu_raw_gyro_scale_factor_     << "' "
                      << "accel scale= "  << imu_raw_accel_scale_factor_);
    }

    void publishInsPVAXMsg(Oem7RawMessageIf::ConstPtr msg)
    {
      std::shared_ptr<novatel_oem7_msgs::msg::INSPVAX> inspvax;
      MakeROSMessage(msg, inspvax);

      inspvax_pub_->publish(inspvax);
    }

    void publishCorrImuMsg(Oem7RawMessageIf::ConstPtr msg)
    {
      MakeROSMessage(msg, corrimu_);
      corrimu_pub_->publish(corrimu_);
    }


    void publishImuMsg()
    {
      if(!imu_pub_->isEnabled() || !inspva_ || imu_rate_ == 0)
        return;

      std::shared_ptr<sensor_msgs::msg::Imu> imu(new sensor_msgs::msg::Imu);

      // Azimuth: Oem7 (North=0) to ROS (East=0), using Oem7 LH rule
      static const double ZERO_DEGREES_AZIMUTH_OFFSET = 90.0;
      double azimuth = inspva_->azimuth - ZERO_DEGREES_AZIMUTH_OFFSET;

      // Conversion to quaternion addresses rollover.
      // Pitch and azimuth are adjusted from Y-forward, LH to X-forward, RH.
      tf2::Quaternion tf_orientation;
      tf_orientation.setRPY(
                         degreesToRadians(inspva_->roll),
                        -degreesToRadians(inspva_->pitch),
                        -degreesToRadians(azimuth)); // Oem7 LH to ROS RH rule

      imu->orientation = tf2::toMsg(tf_orientation);

      if(corrimu_ && corrimu_->imu_data_count > 0)
      {
        double instantaneous_rate_factor = imu_rate_ / corrimu_->imu_data_count;

        imu->angular_velocity.x =  corrimu_->roll_rate  * instantaneous_rate_factor;
        imu->angular_velocity.y = -corrimu_->pitch_rate * instantaneous_rate_factor;
        imu->angular_velocity.z =  corrimu_->yaw_rate   * instantaneous_rate_factor;

        imu->linear_acceleration.x =  corrimu_->longitudinal_acc * instantaneous_rate_factor;
        imu->linear_acceleration.y = -corrimu_->lateral_acc      * instantaneous_rate_factor;
        imu->linear_acceleration.z =  corrimu_->vertical_acc     * instantaneous_rate_factor;
      }

      if(insstdev_)
      {
        imu->orientation_covariance[0] = std::pow(insstdev_->pitch_stdev,   2);
        imu->orientation_covariance[4] = std::pow(insstdev_->roll_stdev,    2);
        imu->orientation_covariance[8] = std::pow(insstdev_->azimuth_stdev, 2);
      }

      // TODO hard-coded for now. Should be set in the oem7_imu.cpp as product-specific values.
      const double LINEAR_COV = 0.0009;
      const double ANGULAR_COV = 0.00035;
      // these are 3x3 matrices, so in 0, 4, 8 spots
      // C 0 0
      // 0 C 0
      // 0 0 C
      imu->angular_velocity_covariance[0] = ANGULAR_COV;
      imu->angular_velocity_covariance[4] = ANGULAR_COV;
      imu->angular_velocity_covariance[8] = ANGULAR_COV;
      imu->linear_acceleration_covariance[0] = LINEAR_COV;
      imu->linear_acceleration_covariance[4] = LINEAR_COV;
      imu->linear_acceleration_covariance[8] = LINEAR_COV;

      imu_pub_->publish(imu);
    }

    void publishInsStDevMsg(Oem7RawMessageIf::ConstPtr msg)
    {
      MakeROSMessage(msg, insstdev_);
      insstdev_pub_->publish(insstdev_);
    }

    /**
     * @return angular velocity, rad / sec
     */
    inline double computeAngularVelocityFromRaw(const double& raw_gyro, const double& bias_iu = 0.0)
    {
      return (raw_gyro - bias_iu) * imu_raw_gyro_scale_factor_;
    }

    /**
     * @return linear acceleration, m / sec^2
     */
    inline double computeLinearAccelerationFromRaw(const double& raw_acc, const double& bias_iu = 0.0)
    {
      return (raw_acc - bias_iu) * imu_raw_accel_scale_factor_;
    }

    void processRawImuMsg(Oem7RawMessageIf::ConstPtr msg)
    {
      // since RAWIMUS has reduced content without benefits in size,
      // we should always use the eXtended version (RAWIMUSX).
      const RAWIMUSXMem* raw;
      
      if (init_raw_calibration_lin_ || init_raw_calibration_ang_) {
        raw = reinterpret_cast<const RAWIMUSXMem*>(msg->getMessageData(OEM7_BINARY_MSG_SHORT_HDR_LEN));
        doInitRawCalibration(*raw);
        if (!raw_imu_pub_->isEnabled()          ||
            imu_rate_                   == 0    ||
            imu_raw_gyro_scale_factor_  == 0.0  ||
            imu_raw_accel_scale_factor_ == 0.0)
          return;
        // upon calibration done, the values should see a small jump.
      } else if (!raw_imu_pub_->isEnabled()   ||
          imu_rate_                   == 0    ||
          imu_raw_gyro_scale_factor_  == 0.0  ||
          imu_raw_accel_scale_factor_ == 0.0) {
        return;
      } else {
        raw = reinterpret_cast<const RAWIMUSXMem*>(msg->getMessageData(OEM7_BINARY_MSG_SHORT_HDR_LEN));
      }
        
      std::shared_ptr<sensor_msgs::msg::Imu> imu = std::make_shared<sensor_msgs::msg::Imu>();
      // All measurements are in sensor frame, uncorrected for gravity. There is no up, forward, left;
      // x, y, z are nominal references to enclsoure housing.
      imu->angular_velocity.x =  computeAngularVelocityFromRaw(raw->x_gyro, bias_raw_imu_rot_[0]);
      imu->angular_velocity.y = -computeAngularVelocityFromRaw(raw->y_gyro, bias_raw_imu_rot_[1]); // Refer to RAWIMUSX documentation
      imu->angular_velocity.z =  computeAngularVelocityFromRaw(raw->z_gyro, bias_raw_imu_rot_[2]);

      imu->linear_acceleration.x =  computeLinearAccelerationFromRaw(raw->x_acc, bias_raw_imu_acc_[0]);
      imu->linear_acceleration.y = -computeLinearAccelerationFromRaw(raw->y_acc, bias_raw_imu_acc_[1]);  // Refer to RAWIMUSX documentation
      imu->linear_acceleration.z =  computeLinearAccelerationFromRaw(raw->z_acc, bias_raw_imu_acc_[2]);

      imu->angular_velocity_covariance[0]    = DATA_NOT_AVAILABLE;
      imu->linear_acceleration_covariance[0] = DATA_NOT_AVAILABLE;
      imu->orientation_covariance[0] = DATA_NOT_AVAILABLE;

      raw_imu_pub_->publish(imu);
    }


  public:
    INSHandler():
      imu_rate_(0),
      imu_raw_gyro_scale_factor_ (1.0),
      imu_raw_accel_scale_factor_(1.0)
    {
    }

    ~INSHandler()
    {
    }

    void initialize(rclcpp::Node& node)
    {
      node_ = &node;

      imu_pub_       = std::make_unique<Oem7RosPublisher<sensor_msgs::msg::Imu>>(            "IMU",       node);
      raw_imu_pub_   = std::make_unique<Oem7RosPublisher<sensor_msgs::msg::Imu>>(            "RAWIMU",    node);
      corrimu_pub_   = std::make_unique<Oem7RosPublisher<novatel_oem7_msgs::msg::CORRIMU>>(  "CORRIMU",   node);
      insstdev_pub_  = std::make_unique<Oem7RosPublisher<novatel_oem7_msgs::msg::INSSTDEV>>( "INSSTDEV",  node);
      inspvax_pub_   = std::make_unique<Oem7RosPublisher<novatel_oem7_msgs::msg::INSPVAX>>(  "INSPVAX",   node);
      insconfig_pub_ = std::make_unique<Oem7RosPublisher<novatel_oem7_msgs::msg::INSCONFIG>>("INSCONFIG", node);

      DriverParameter<int> imu_rate_p("oem7_imu_rate", 0, *node_);
      imu_rate_ = imu_rate_p.value();
      if(imu_rate_ > 0)
      {
        RCLCPP_INFO_STREAM(node_->get_logger(), "INS: IMU rate overriden to " << imu_rate_);
      }

      init_raw_calibration_lin_ = node_->declare_parameter<bool>("ins_raw_static_init_linear_calib", false);
      init_raw_calibration_ang_ = node_->declare_parameter<bool>("ins_raw_static_init_angular_calib", false);
    }

    const MessageIdRecords& getMessageIds()
    {
      static const MessageIdRecords MSG_IDS(
                                      {
                                        {RAWIMUSX_OEM7_MSGID,            MSGFLAG_NONE},
                                        {CORRIMUS_OEM7_MSGID,            MSGFLAG_NONE},
                                        {IMURATECORRIMUS_OEM7_MSGID,     MSGFLAG_NONE},
                                        {INSPVAS_OEM7_MSGID,             MSGFLAG_NONE},
                                        {INSPVAX_OEM7_MSGID,             MSGFLAG_NONE},
                                        {INSSTDEV_OEM7_MSGID,            MSGFLAG_NONE},
                                        {INSPVAS_OEM7_MSGID,             MSGFLAG_NONE},
                                        {INSCONFIG_OEM7_MSGID,           MSGFLAG_STATUS_OR_CONFIG},
                                      }
                                    );
      return MSG_IDS;
    }

    void handleMsg(Oem7RawMessageIf::ConstPtr msg)
    {
      switch(msg->getMessageId()){
        case INSPVAS_OEM7_MSGID:
          MakeROSMessage(msg, inspva_); // Cache
          break;
        case INSSTDEV_OEM7_MSGID:
          publishInsStDevMsg(msg);
          break;
        case CORRIMUS_OEM7_MSGID:
        case IMURATECORRIMUS_OEM7_MSGID:
          publishCorrImuMsg(msg);
          publishImuMsg();
          break;
        case INSCONFIG_OEM7_MSGID:
          processInsConfigMsg(msg);
          break;
        case INSPVAX_OEM7_MSGID:
          publishInsPVAXMsg(msg);
          break;
        case RAWIMUSX_OEM7_MSGID:
          processRawImuMsg(msg);
          break;
        default:
          assert(false);
          break;
      }
    }
  };

}

#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS(novatel_oem7_driver::INSHandler, novatel_oem7_driver::Oem7MessageHandlerIf)
